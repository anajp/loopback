"use strict";

module.exports = function (Person) {

    Person.getName = function (personId, cb) {
        Person.findById(personId, function (err, instance) {
            var response = "Name of person is " + instance.name;
            cb(null, response);
            console.log(response);
        });
    }

    Person.remoteMethod(
        "getName",
        {
            http: { path: "/getname", verb: "get" },
            accepts: { arg: "id", type: "string", http: { source: "query" } },
            returns: { arg: "name", type: "string" }
        }
    );

};
